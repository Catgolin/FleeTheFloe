﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recyclage : MonoBehaviour {

	public GameObject solPrefab;
	public GameObject obstaclePrefab;
	public GameObject bonusPrefab;
	public float largeur;

	private Transform joueur;
	private bool spawn = false;
	private int nbreObstacles = 0;

	void Start() {
		this.joueur = GameObject.FindGameObjectsWithTag("Player")[0].transform;
		this.nbreObstacles = (int)(this.transform.localScale.x / this.obstaclePrefab.transform.localScale.x);
		this.nbreObstacles--;
	}

	// Override
	void Update () {
		if(!this.spawn && this.transform.position.z < this.joueur.position.z + 10*this.transform.localScale.z) {
			float decalage = Random.Range(-this.transform.localScale.x/2, this.transform.localScale.x/2);
			Instantiate(solPrefab, this.transform.position+ new Vector3(decalage, -1, this.transform.localScale.z-1), Quaternion.identity);
			for(int i = 0; i < this.transform.localScale.z; i+= 10*(int)this.obstaclePrefab.transform.localScale.z) {
				if(Random.Range(0, 1) < 0.5) {
					float x = Random.Range(-0.5f*this.largeur
						, 0.5f*this.largeur);
					Vector3 pos = new Vector3(x, 21, (float)i)+this.transform.position;
					Instantiate(obstaclePrefab, pos, Quaternion.identity);
				}
				if(Random.Range(0, 1) < 0.25) {
					float x = Random.Range(-0.5f*this.largeur-decalage+2, 0.5f*this.largeur+decalage-2);
					Vector3 pos = this.transform.position + new Vector3(x, 2f, (float)(i-20));
					Instantiate(this.bonusPrefab, pos, Quaternion.identity);
				}
			}
			this.spawn = true;
		}
	}
}
