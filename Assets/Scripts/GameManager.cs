using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public Canvas canvas;
	public bool alive = true;
	private float temps = 15;
	private Animator anim;
	void Start() {
		this.anim = this.canvas.GetComponent<Animator>();
	}

	public float getTemps() {
		return this.temps;
	}

	public void ajouterTemps() {
		if(this.alive) {
			this.anim.Play("PlusUn");
			this.temps += 1;
		}
	}

	public void TerminerJeu() {
		this.alive = false;
		this.anim.SetFloat("Fin", 1);
	}

	public void Restart() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	void Update() {
		this.temps -= Time.deltaTime;
	}

	public void Quitter() {
		Application.Quit();
	}
}
