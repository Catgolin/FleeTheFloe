﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMouvement : MonoBehaviour {

	public Rigidbody corps;
	public float acceleration;
	public float accelerationLaterale;
	public float vitesseSaut;

	private float tourner;
	private bool sauter;
	private bool piedSurGlace = true;

	// Override
	void Update() {
		this.tourner = Input.GetAxis("Horizontal");
		this.sauter = Input.GetKeyDown(KeyCode.Space);
	}

	// Override
	void FixedUpdate () {
		if(FindObjectOfType<GameManager>().alive) {
			this.corps.AddForce(0, 0, this.acceleration * Time.deltaTime);
			float vitesseLaterale = this.accelerationLaterale * Time.deltaTime * this.tourner;
			this.transform.position += new Vector3(vitesseLaterale, 0f, 0f);
			if(this.tourner != 0) {
				this.corps.velocity = new Vector3(0, this.corps.velocity.y, this.corps.velocity.z);
			}
			Quaternion target = Quaternion.Euler(-90,0,180);
			transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 10f);
			if(this.sauter && this.piedSurGlace) this.corps.velocity += Vector3.up * this.vitesseSaut;
		}
	}

	void OnCollisionExit(Collision c) {
		if(c.gameObject.tag == "Sol") this.piedSurGlace = false;
	}
	void OnCollisionEnter(Collision c) {
		if(c.gameObject.tag == "Sol") this.piedSurGlace = true;
	}
}
