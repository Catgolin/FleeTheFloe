﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour {
	public AudioSource valider;

	private GameManager gm;
	private bool valide;

	void Start() {
		this.gm = FindObjectOfType<GameManager>();
		this.valide = false;
	}
	void OnTriggerEnter(Collider other) {
		if(!this.valide) {
			if(other.tag == "Player") {
				this.gm.ajouterTemps();
				this.valider.Play();
				this.valide = true;
				Destroy(this.gameObject, 0.65f);
			} else {
				Destroy(this.gameObject);
			}
		}
	}
}
