﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

	public Transform joueur;
	public Vector3 position;

	// Override
	void Update () {
		transform.position = this.joueur.position + this.position;
	}
}
