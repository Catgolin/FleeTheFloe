﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public Transform joueur;
	public Text affichage;
	public Text horloge;
	public float temps;

	private GameManager gameManager;

	void Start() {
		this.gameManager = FindObjectOfType<GameManager>();
	}

	// Update is called once per frame
	void Update () {
		if(this.gameManager.getTemps() <= 0) {
			this.gameManager.TerminerJeu();
			this.horloge.text = "Temps écoulé";
		} else {
			this.horloge.text = this.gameManager.getTemps().ToString("0");
		}
		this.affichage.text = "\nScore : "+(this.joueur.position.z/10).ToString("0");
	}
}
